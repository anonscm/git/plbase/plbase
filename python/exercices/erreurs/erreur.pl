author=DR
template= example
text==
Une des difficultés de la programmation vient du fait que l'ordinateur ne comprend pas mais exécute simplement vos instructions.
Hors si celle ci n'ont pas de *sens* , si votre programme n'est pas *correct* , le programme n'est pas executable.    
Le code qui vous est proposé dans l'éditeur contient une erreur. Lancez l'execution du code pour voir l'erreur.
Puis corrigez celle ci.
==
hint==
Un des problèmes difficile a résoudre est la bonne analyse des erreurs de programmation par le langage (ici l'interpréteur).
C'est pourquoi très souvent l'erreur est détecté sur la ligne N mais signalée sur la ligne N+1.
Donc si votre message d'erreur est "syntaxe error ligne 33" et que la ligne 33 vous semble totalement légitime regardez donc la lignge 32 qui contient sans doute l'erreur.
==
