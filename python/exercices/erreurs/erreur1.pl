author=DR
template= example # même heritage
text==
Une des difficultés de la programmation vient du fait que l'ordinateur ne comprend pas mais exécute simplement vos instructions.
Hors si celle ci n'ont pas de *sens* , si votre programme n'est pas *correct* , le programme n'est pas executable.    
Le code qui vous est proposé dans l'éditeur contient une erreur. Lancez l'execution du code pour voir l'erreur.
Puis corrigez celle ci.
==
code==
print("hello python student")
 print(" il faut banir les tabulations")
==
hint==
La position du premier caractère visible (c'est à dire ni espace, ni tabulation) dans une ligne de texte est appelée 
*l'indentation* de la ligne. Cette indentation indique à python le regroupement des instructions dans les blocs d'instruction 
Dans le cas présent l'erreur *IndentationError: unexpected indent* indique que la ligne précdente ne démare pas un bloc d'instruction et
donc que la ligne doit être sur la même indentation.
==
