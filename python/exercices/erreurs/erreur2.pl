author=DR
template=erreur
# le code suivant contien une erreur c'est volotaire
code==
print(int(y)
print("hello")
==
hint==
Souvent l'erreur est détecté sur la ligne N mais signalée sur la ligne N+1.
Donc si votre message d'erreur est *syntaxe error ligne 33* 
et que la ligne 33 vous semble totalement légitime regardez donc la ligne 32 qui contient sans doute l'erreur.
==
