author=DR
# template=None
# conditionelle
#

text==
Votre programme doit lire un entier au clavier (entrée standard),
puis en fonction de la valeur lue, afficher l'état de l'eau (H2O).
Négatif glace
Zéro point de fusion
zéro à Cent eau liquide
Cent point d'évaporation
Au dessus de Cent vapeur d'eau
==

code==
# éléments à utiliser
print("glace")
print("point de fusion")
print("eau liquide")
print("point d'évaporation")
print("vapeur d'eau")
==
solver=@python/exercices/conditionnelles/condsolver.py


solution=@soluce.py
inputs=@inputs


