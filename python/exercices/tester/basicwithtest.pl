author=DR|NB
# modalités de l'environement de test fournis par Nicolas
# merci nicolas
template=None
# ceci est un solver utilisant une liste input / output
# a comperare avec le code de l'élève
tag=iterable|function|boucle
difficulty=hard
text==
<p color="red">
Implementez !</p> une fonction `max` qui prend en paramètre un itérable
et retourne le max des éléments accessible par l'itérable.
Si il n'y a pas d'éléments la fonction retourne `None`.
==
code==
# taper ici
==
tester==
import student
import doctest
doctest.testfile("./test.py")
==

grader==
print("bravo")
==
files=test.py
