author=DR
template=basicwithtest
text==
La suite de Fibonacci est une suite d'entiers dans laquelle chaque terme est la somme des deux termes qui le précèdent.
 Elle commence généralement par les termes 0 et 1 et ses premiers termes sont : 0, 1, 1, 2, 3, 5, 8, 13, 21,...

Écrire une fonction `fibo` qui calcul le Nième terme de la suite. Par exemple:
```
>>> fibo(6)
8
```
==


test==
"""
>>> fibo(0)
0
>>> fibo(1)
1
>>> fibo(6)
8
>>> fibo(-1)
None
"""
===
