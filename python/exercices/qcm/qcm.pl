
author=DR
# les qcm sont des questions à choix multiple
template=/python/exercices/solver/solver
text==
Qui est président de la république à l'inauguration du canal de suez le 17 novembre 1869.

A Personne (Nemo)
B Charles De Gaulle
C Georges Pompidou
D Warren G. Harding
==
code==
print("A")
print("B")
print("C")
print("D")
==

solution==
print("A")
==

====
#une autre version qui fonction avec un grader
template=None # le grader doit être construit de façon addoc mais le modele est simple cf plus bas
text==
une question qcm a la noix la bonne réponce est la réponse A
saisisez la bonne réponse dans l'éditeur
==
# code est vide 
grader==
f=open("student.py")
q=f.read()
q=q.strip()
if q=="A":
	print("bonne réponse")
	exit(0)
else:
	import sys
	print("mauvaise réponse",file=sys.stderr)
	exit(1)
## fin du grader
==
===
#un autre exercice qui demande une réponse sous forme de chaine de caractère
text==
Veuillez saissir un mot qui contient deux fois plus de A que de B et trois fois plus de A que de C.
Par example DDTEDDTE contient deux fois plus de D que de T et deux fois plus de D que de E.
==
grader==
def compte(c,s):
	n=0
	for u in s:
		if u==c:
			n=n+1
	return n
f=open("student.py")
q=f.read()
q=q.strip()
A = compte('A',q)
B = compte('B',q)
C = compte('C',q)

if ( A== B * 2 ) and ( A == C * 3) :
	print("bonne réponse")
	exit(0)
else:
	import sys
	print("mauvaise réponse",file=sys.stderr)
	exit(1)
## fin du grader
==
