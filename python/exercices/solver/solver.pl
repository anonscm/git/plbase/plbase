
author=DR
template=None
text==
Afficher solver
ou
Afficher solver sur la sortie standard
==
solution==
print("solver")
==

template=None # basic template

solver==
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  SOLVER BASIC compare output of the import of the
#  "./student.py"  module and the
#   "./solution.py" module
#  Copyright 2015 Dominique Revuz <dr@u-pem.fr>
#
import importlib
import unittest
from io import StringIO
import sys

def domodule( name):
    old_stdout = sys.stdout
    sys.stdout = tmpstdout = StringIO()
    i = importlib.import_module(name)
    sys.stdout =    old_stdout
    return tmpstdout

if __name__ == '__main__':
    unittest.TestCase().assertEqual(domodule("student").getvalue(),domodule("solution").getvalue())
else:
    raise(" must not be imported ")
==
