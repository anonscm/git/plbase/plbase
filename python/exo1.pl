template = default.pl bf1c17440b73db48a0dd9b19130ce033eb053801
text == # le texte qui est affiché à l'apprenant
Votre premier exercice python affichez quelque chose à l'écran
pour cela écrivez une ligne comme celle ci :
print(" le texte de votre choix")
une fois l'exécution réalisé vous pourrer passer à l'exercice suivant
sinon rappel nous étudions python 3
==
code==
# ce texte est mis comme valeur par defaut du widget d'édition de code
# et donc apparait dans la fenêtre d'edition
# tapez dans la ligne suivante le code print(" hello world ")
==
