template = default.pl
text == # le texte qui est affiché à l'apprenant

Ecrire une fonction double qui retourne le double de son paramètre 

puis lisez un enter et appeler la avec cet entier 
une fois l'exécution réalisé vous pourrer passer à l'exercice suivant
==
code==
ent = int(input("Taper un entier"))
print(double(ent)))
==
soluce==
def double(val):
	return val*2
ent = int(input("Taper un entier"))
print(double(ent)))
==
