
### Format direct : la page wp

* la question est pour GB de savoir si il souhaite avoir un accès moodle pour ASP. Si oui il faut ce format.*

Le format simplifié existe de façon a pouvoir utiliser des pages wppy directement, ce sont des pages du site wordpress sans transformation.
La syntaxe est celle du plugin pybox (cf. pybox_syntaxe), attention aux instructions _files_ qui necessite de lire un autre fichier.

Une commande permet d'extraire les pybox contenus dans une page pour les partagées, il faut qu'un slug unique soit défini sinon pas d'extraction.

Sous moodle ce type de page est une _activité_ qui s'ajoute dans un cours ordinaire et ou les exercices de la page sont fait (ou pas) dans l'ordre que l'élèves souhaite.
Ces pages sont utilisées pour faire de la formation a distance (mooc)


## Format traduit

La liste suivante donne les mots clef du format pybox:

* pyExample
* pyBox
* pyRecall

	permet de définir un template specifique

* pyShort
* pyMulti
* pyMultiScramble
* pyWarn

NOTE: pyWarn c'est comme une note



[source,none]
.Pybox
````
[pyBox
allowinput = "y"
title = "Probabilité d'obtenir un 10 en jouant 2 dés"
slug = "proba_10_2 des"
solver= "
nbLancers = 1
proba10 = 1/12
while nbLancers <= 10**4 :
   gagne = 0
   for i in range (1,nbLancers+1) :
      DeVert = _rint(1,6)
      DeRouge = _rint(1,6)
      if DeVert + DeRouge == 10 :
         gagne += 1
   frequence = gagne/nbLancers
   print (nbLancers,gagne,frequence,frequence - proba10)
   nbLancers *= 5
"
defaultcode = "# Entrez votre code ici"
repeats1 = "1"
]




La probabilité d'obtenir 10 en lançant 2 dés à 6 faces est égale à [pyHint hint="En effet, il y a 6x6=36 sorties possibles et parmi ces 36 sorties, celles qui donnent 10 sont (6,4), (5,5) et (4,6)"]3 / 36[/pyHint], soit 0.083333.[br]
La loi faible des grands nombres de Bernouilli dit que la fréquence d'apparition d'un événement se rapproche de la probabilité lorsque le nombre de tirages est grand.[br]

Cet exercice propose de vérifier cette loi dans le cas où on cherche à obtenir un 10 en lançant 2 dés.[br]

Pour cela on va répéter des parties dans lesquelles on lance successivement 2 dés, avec un nombre de lancers de plus en plus grand. D'abord 1 lancer, puis 5, puis 25, puis 125 et ainsi de suite en multipliant le nombre de lancers par 5 à chaque fois sans dépasser [pyHint hint="Il faudrait pouvoir pousser bien plus loin mais ce site refuse que les programmes s'exécutent en prenant trop de temps."]10 000[/pyHint] lancers.[br]
Quand chaque partie est finie, on affiche une ligne indiquant le nombre de lancers, le nombre de coups gagnants, la fréquence de gain et la différence entre la probabilité et la fréquence de gain.[br][br]
Voilà le format attendu pour la sortie :[br][br]
1 0 0.0 -0.08333333333333333[br]
5 0 0.0 -0.08333333333333333[br]
25 2 0.08 -0.003333333333333327[br]
125 18 0.144 0.06066666666666666[br]
625 48 0.0768 -0.0065333333333333354[br]
3125 272 0.08704 0.0037066666666666775[br][br]
[pyWarn]
Remarque importante : comme ce programme génère des nombres aléatoires, il n'y a aucune chance que votre sortie corresponde à celle du système. Donc votre programme sera toujours évalué incorrect.[br]
Pas de panique ! Ce que vous devez constater dans votre sortie, c'est que la fréquence s'approche de 0.08[br][br]
Si vous voulez tester votre programme plus finement, en augmentant sensiblement le nombre de lancers, faites-le tourner en local sur votre PC personnel et non sur ce site, vous ne serez pas bloqué par des limites de "temps dépassé".[br]
[/pyWarn]
[/pyBox]
````
